from subprocess import call
call("pip install scikit-image --no-cache-dir".split(" "))
call("pip install snowflake-connector-python --no-cache-dir".split(" "))

import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms.functional as TF
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import torch.optim as optim
from PIL import Image
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
import boto3
import io
from sklearn.cluster import KMeans
from sklearn import decomposition
from skimage.transform import resize
import pdb
import datetime
import matplotlib as mpl
from skimage.util import img_as_ubyte
from multiprocessing import pool
import shutil
from torch.optim.lr_scheduler import StepLR
import os
from sklearn.neighbors import KDTree
from numpy import unique
from numpy import where
import snowflake.connector
from io import StringIO 
import argparse
import json
import logging
import sys
import torch.distributed as dist

torch.manual_seed(0)
np.random.seed(0)
s3=boto3.client('s3')
height=28
width=28
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))


class ReadDF():
    """ Read Data from S3/ Snowflake """
    def __init__(self,bucket=None,user=None,password=None):
        self.bucket=bucket
        self.user=user
        self.password=password

    def s3_read(self,path=str,columns=None,nrows=None,compression=None):
        obj = s3.get_object(Bucket=self.bucket, Key=path.strip())
        df = pd.read_csv(io.BytesIO(obj['Body'].read()), index_col='pattern_hash',names=columns,compression=compression,nrows=nrows)
        return df
    
    def filter_df(self,df,x_low,x_high,y_low,y_high):
        df['x_length']= df['x_deltas'].str.split().str.len()
        df['y_length']= df['y_deltas'].str.split().str.len()
        df=df[((df['x_length']>x_low)&(df['x_length']<x_high))&((df['y_length']>y_low)&(df['y_length']<y_high))]
        df=df.drop(['x_length','y_length'],1)
        return df
    
    def snowflake_read(self,columns=list,query=None):
    
        df=pd.DataFrame()
     
        snowflakesparkconfig_prod = {'account': '', 
                                     'user': '', 
                                     'password': '',
                                     'warehouse': '', 
                                     'role': '',
                                     'schema': '', 
                                     'database': '',
                                     'host': ''}
        
        py_ctx1 = snowflake.connector.connect(**snowflakesparkconfig_prod)
        conn1 = py_ctx1.cursor()

        conn1.execute(query)

        hid = conn1.fetchall()#fetchall()#fetchmany(150000)#.fetchall()

        df=pd.concat([pd.DataFrame(data=hid,columns=columns),df])#.set_index('pattern_hash')
        df=df.drop_duplicates(subset='pattern_hash')
        df=df.set_index('pattern_hash')
        return df
        
    def s3_write(self, df, path, name):
        csv_buffer = StringIO()
        df.to_csv(csv_buffer)
        s3_resource = boto3.resource('s3')
        s3_resource.Object(self.bucket,path+'{}.csv'.format(name)).put(Body=csv_buffer.getvalue())

class AverageMeter(object): 
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.encoder =  nn.Sequential(
                        nn.Conv2d(1, 128, kernel_size=3, stride=1, padding=1),
                        nn.MaxPool2d(kernel_size=2, stride=2),
                        nn.Conv2d(128, 96, kernel_size=3, stride=1, padding=1),
                        nn.MaxPool2d(kernel_size=2, stride=2),
                        nn.Conv2d(96, 64, kernel_size=3, stride=1, padding=1),
                        nn.MaxPool2d(kernel_size=2, stride=2),
                        )
        self.decoder =  nn.Sequential(
                        nn.ConvTranspose2d(64, 96, kernel_size=3, stride=1, padding=1),
                        nn.ConvTranspose2d(96, 128, kernel_size=3, stride=1, padding=1),
                        nn.ConvTranspose2d(128, 1, kernel_size=3, stride=1, padding=1),
                        )
        self._initialize_weights2()

    def _initialize_weights2(self):
        for m in self.modules():
            
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                n = m.weight.size(1)
                m.weight.data.normal_(0, 0.01)
                if m.bias is not None:
                    m.bias.data.zero_()

    def forward(self, x):
        #encoder
        x = F.relu(self.encoder(x))
        x = self.pool(x)
        
        print (x)
        
        #decoder
        x = F.relu(self.decoder(x))
        x = F.sigmoid(x)
        return out



class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.features   = nn.Sequential(
                        nn.Conv2d(1, 128, kernel_size=3, stride=1, padding=1),
                        nn.BatchNorm2d(128),
                        nn.ReLU(inplace=True),
                        nn.MaxPool2d(kernel_size=2, stride=2),
                        nn.Conv2d(128, 96, kernel_size=3, stride=1, padding=1),
                        nn.BatchNorm2d(96),
                        nn.ReLU(inplace=True),
                        nn.MaxPool2d(kernel_size=2, stride=2),
                        nn.Conv2d(96, 64, kernel_size=3, stride=1, padding=1),
                        nn.BatchNorm2d(64),
                        nn.ReLU(inplace=True),
                        nn.MaxPool2d(kernel_size=2, stride=2),
                        nn.Conv2d(64, 32, kernel_size=3, stride=1, padding=1),
                        nn.BatchNorm2d(32),
                        nn.ReLU(inplace=True),
                        nn.MaxPool2d(kernel_size=2, stride=2),
                        )
        self.deconv1 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        self.deconv2 = nn.Conv2d(64, 96, kernel_size=3, stride=1, padding=1)
        self.deconv3 = nn.Conv2d(96, 128, kernel_size=3, stride=1, padding=1)
        self.deconv4 = nn.Conv2d(128, 1, kernel_size=3, stride=1, padding=1)
        
        self._initialize_weights2()

    def _initialize_weights2(self):
        for m in self.modules():
            
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                n = m.weight.size(1)
                m.weight.data.normal_(0, 0.01)
                if m.bias is not None:
                    m.bias.data.zero_()

    def forward(self, x):
        
        out = self.features(x)
        #pdb.set_trace()
        #out = F.relu(self.deconv1(out))
        out = F.interpolate(out, scale_factor=2, mode='nearest')
        #out = F.relu(self.deconv2(out))
        #out = F.interpolate(out, scale_factor=2, mode='nearest')
        out = F.relu(self.deconv1(out))
        out = F.interpolate(out, size=(7,7), mode='nearest')
        out = F.relu(self.deconv2(out))
        out = F.interpolate(out, scale_factor=2, mode='nearest')
        out = F.relu(self.deconv3(out))
        out = F.interpolate(out, scale_factor=2, mode='nearest')
        out = torch.sigmoid(self.deconv4(out))
        return out
                



def mulTopoSig(topoSig,lyrnum):
    #pdb.set_trace()
    topoSig = topoSig.split()
    TopoSigList = ['' for x in range(lyrnum)]
    while (len(topoSig) > 0):
        nextBit = int(topoSig.pop(0))
        binBit = (bin(nextBit)[2:])
        binBitL = len(binBit)
        binBit = binBit[::-1]
        for i in range(lyrnum):
            if (i < binBitL):
                TopoSigList[i] = TopoSigList[i]+ binBit[i]
            else:
                TopoSigList[i] = TopoSigList[i]+ '0'
    return(TopoSigList)

def string_to_numpy(topo_sig_list, xsd, ysd):
    xsd_np = np.array(xsd.split()).astype(np.int)
    ysd_np = np.array(ysd.split()).astype(np.int)
    topo_sig_np_list = []
    for topo_sig in topo_sig_list:
        topo_sig_np = np.fromstring(' '.join(topo_sig), dtype=int, sep = ' ').reshape((len(ysd_np),len(xsd_np)))
        topo_sig_np_list.append(topo_sig_np)
    return topo_sig_np_list, xsd_np, ysd_np

def get_unsquished_array(topo_sig_np_list, xsd, ysd):
    unsquished_array_list = []
    for topo_sig_np in topo_sig_np_list:
        unsquished_array = np.zeros((ysd.sum(), xsd.sum()))
        ysd_rev = ysd[::-1]
        rows, columns = np.where(topo_sig_np == 1)
        for row, column in zip(rows, columns):
            row_start_index = sum(ysd_rev[:row])
            row_end_index = sum(ysd_rev[:row])+ysd_rev[row]
            column_start_index = sum(xsd[:column])
            column_end_index = sum(xsd[:column])+xsd[column]
            unsquished_array[row_start_index:row_end_index, column_start_index:column_end_index] = 1
        unsquished_array_list.append(unsquished_array)
    return unsquished_array_list

def show_pattern(record_of_interest):
    
    toposig = record_of_interest.topology_signature
    toposiglist = mulTopoSig(toposig,3)
    xsd = record_of_interest.x_deltas
    ysd = record_of_interest.y_deltas
    topo_sig_np_list, xsd_np, ysd_np = string_to_numpy(toposiglist, xsd, ysd)
    two_channel_img = np.array(get_unsquished_array(topo_sig_np_list, xsd_np, ysd_np))
    two_channel_img = img_as_ubyte(two_channel_img)
    two_channel_img = two_channel_img.transpose(1,2,0)
    gray_img = np.dot(two_channel_img[...,:3], [0.299, 0.587, 0.114])
    return img_as_ubyte(two_channel_img)#gray_img.astype(np.uint8)

class PatternHashDataset(Dataset):
    
    def __init__(self, dataframe, transform=None):
        self.df = dataframe
        self.transform = transform
    
    def __len__(self):
        return(self.df.shape[0])

    def __getitem__(self,idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        image = show_pattern(self.df.iloc[idx,:])
        #image = np.uint8(image)*255
        #pdb.set_trace()
        if self.transform:
            image = self.transform(image)

        return image

class Resize():
    def __call__(self, sample):
        image = resize(sample, (height, width,1))
        return img_as_ubyte(image)

def plot_filters(model):
    fig, ax = plt.subplots(4,32, figsize=(20,10))
    row = -1
    for module in model.modules():
        if isinstance(module, nn.Conv2d):
            #pdb.set_trace()
            row = row+1
            for i in range(module.weight.shape[0]):
                ax[row][i].imshow(module.weight[i,0].squeeze().detach().cpu())
                ax[row][i].axes.xaxis.set_visible(False)
                ax[row][i].axes.yaxis.set_visible(False)
                plt.pause(1)
                plt.show()
                exit()
    #pdb.set_trace()

def train_transforms(window_size,train=False,view=False,encodings=False):
    if train==True:
        transform = transforms.Compose([  
                                transforms.ToPILImage(),
                                transforms.Grayscale(num_output_channels=1),
                                transforms.RandomHorizontalFlip(),
                                transforms.RandomVerticalFlip(),
                                transforms.TenCrop(window_size), 
                                transforms.Lambda(lambda crops:[transforms.Resize(size=(28,28))(crop) for crop in crops]),
                                transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops]))
                                ])
    if view==True:
        transform = transforms.Compose([  
                                transforms.ToPILImage()
                                
                                ])
    if encodings==True:
        transform = transforms.Compose([
                            transforms.ToPILImage(),
                            transforms.CenterCrop(350),
                            transforms.Grayscale(num_output_channels=1),
                            transforms.Resize(size=((28,28))),
                            transforms.ToTensor()
                            ])
    return transform

class Encoder():
    def __init__(self,window_size):
        self.window_size=window_size

    def save_ckp(self,state, epoch, checkpoint_dir,batch):
        f_path = checkpoint_dir +'/checkpoint_epoch:{}_batch:{}.pt'.format(epoch,batch)
        torch.save(state, f_path)

    def load_ckp(self,checkpoint_fpath, model, optimizer):
        checkpoint = torch.load(checkpoint_fpath)
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        return model, optimizer, checkpoint['epoch']

    def initialize_model(self,lr=1e-3,wd=1e-4,load_model=False,path=None):
        model = Model()
        if torch.cuda.is_available():
            model = model.cuda()
        optimizer   = optim.Adam(model.parameters(), lr=lr, weight_decay=wd)
        #scheduler = StepLR(optimizer, step_size=1, gamma=0.1)
        start_epoch=0
        if load_model==True:
            ckp_path=path
            model, optimizer, start_epoch= self.load_ckp(ckp_path, model, optimizer)
        return {'model':model,'optimizer':optimizer,'start_epoch':start_epoch,}
        

    def train_model(self, df, args):
        is_distributed = len(args.hosts) > 1 and args.backend is not None
        logger.debug("Distributed training - {}".format(is_distributed))
        use_cuda = args.num_gpus > 0
        logger.debug("Number of gpus available - {}".format(args.num_gpus))
        kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
        device = torch.device("cuda" if use_cuda else "cpu")

        if is_distributed:
            world_size = len(args.hosts)
            os.environ['WORLD_SIZE'] = str(world_size)
            host_rank = args.hosts.index(args.current_host)
            os.environ['RANK'] = str(host_rank)
            dist.init_process_group(backend=args.backend, rank=host_rank, world_size=world_size)
            logger.info('Initialized the distributed environment: \'{}\' backend on {} nodes. '.format(
                args.backend, dist.get_world_size()) + 'Current host rank is {}. Number of gpus: {}'.format(
                dist.get_rank(), args.num_gpus))

        # set the seed for generating random numbers
        torch.manual_seed(args.seed)
        if use_cuda:
            torch.cuda.manual_seed(args.seed)

        if args.model_path == None:
            model = Model()
        else:
            s3 = boto3.resource('s3')
            my_bucket = s3.Bucket('gf-global-dev-mfg-adapt-bucket')
            my_bucket.download_file(args.model_path, 'model.pth')
        model = torch.load('model.pth')
            
            
        if torch.cuda.is_available():
            model = model.cuda()
        optimizer   = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.wd)
        #scheduler = StepLR(optimizer, step_size=1, gamma=0.1)
        start_epoch=0
        """if load_model==True:
            ckp_path=path
            model, optimizer, start_epoch= self.load_ckp(ckp_path, model, optimizer)
          """  
        if is_distributed and use_cuda:
            # multi-machine multi-gpu case
            model = torch.nn.parallel.DistributedDataParallel(model)
        else:
            # single-machine multi-gpu case or single-machine or multi-machine cpu case
            model = torch.nn.DataParallel(model)

        if not os.path.exists('output'):
            os.mkdir('output')
        else:
            print('Checkpoint Directory: output/')
        checkpoint_dir='output'
        start = datetime.datetime.now()
        losses = AverageMeter('loss')
        fig,ax=plt.subplots(1,2,figsize=(15,15))
        #model.train()
        for epoch in range(1, args.epochs + 1):
            model.train()
            temp_df = df.sample(frac=args.sample_factor)
            data = PatternHashDataset(temp_df, transform=train_transforms(window_size=self.window_size, train=True))
            dataloader = DataLoader(data, batch_size=args.batch_size, shuffle=False, **kwargs)

            """logger.debug("Processes {}/{} ({:.0f}%) of train data".format(
                len(train_loader.sampler), len(train_loader.dataset),
                100. * len(train_loader.sampler) / len(train_loader.dataset)
            ))

            logger.debug("Processes {}/{} ({:.0f}%) of test data".format(
                len(test_loader.sampler), len(test_loader.dataset),
                100. * len(test_loader.sampler) / len(test_loader.dataset)
            ))"""
            for batch, images in enumerate(dataloader):
                #ind = df.iloc[batch*batch_size:(batch*batch_size)+batch_size].index
                if torch.cuda.is_available():
                    images = images.cuda()
                    images = images.view(-1, 1, 28, 28)
                output = model(images)
                optimizer.zero_grad()
                loss = F.mse_loss (output, images)
                loss.backward()
                if is_distributed and not use_cuda:
                    # average gradients manually for multi-machine cpu case only
                    size = float(dist.get_world_size())
                    for param in model.parameters():
                        dist.all_reduce(param.grad.data, op=dist.reduce_op.SUM)
                        param.grad.data /= size
                optimizer.step()
                losses.update(loss.item(), images.shape[0])
                if batch%10==0:
                    print('Epoch:{}, Batch: {:.0f}, train loss: {:.3e} time: {:.1f} min'.format(epoch,batch, losses.avg, (datetime.datetime.now()-start).total_seconds()/60))
                    ax[0].imshow(np.transpose(images[0].cpu().numpy(),(1,2,0)))
                    ax[1].imshow(np.transpose(output[0].detach().cpu().numpy(),(1,2,0)))
        return model
            

    def get_encodings(self,df,model_config,batch_size=256,num_workers=6):
        model=model_config['model']
        df_final=pd.DataFrame()
        data=PatternHashDataset(df,transform=train_transforms(self.window_size,encodings=True))
        dataloader=DataLoader(data, batch_size=batch_size, shuffle=False, num_workers=num_workers)

        model.eval()
        for batch, images in enumerate(dataloader):
            ind=df.iloc[(batch*batch_size):(batch*batch_size)+batch_size].index
            if torch.cuda.is_available():
                images = images.cuda()
            feat=model.features(images).detach().cpu().numpy()
            feat=pd.DataFrame(feat.reshape(feat.shape[0], -1))
            feat['pattern_hash']=ind
            feat=feat.set_index('pattern_hash')
            df_final=pd.concat([feat,df_final],0)
        return df_final

    def get_similar_patterns(self,df,k=None,pattern_hash=None):
        pd.options.mode.chained_assignment = None
        tree=KDTree(df)
        dist,ind=tree.query(df[df.index==pattern_hash],k=k)
        patterns=df.iloc[ind.tolist()[0]]
        patterns['distance']=dist[0]
        patterns=patterns.sort_values('distance')
        return patterns

    
    

def train(args):
    columns = ['pattern_hash','topology_signature','x_deltas','y_deltas']
    print(args.data_dir)
    df = pd.read_csv(args.data_dir+'/M1i1processed_final.csv', names=columns)
    encoder = Encoder(350)
    model = encoder.train_model(df,args)
    return model

def save_model(model, bucket, path, name):
    buffer = io.BytesIO()
    torch.save(model, buffer)
    s3.put_object(Bucket=bucket, Key=path + '/' + name, Body=buffer.getvalue())
    
def model_fn():
    #https://sagemaker.readthedocs.io/en/stable/frameworks/pytorch/using_pytorch.html#deploy-pytorch-models
    #Deployment yet to be implemented custom inference script need to be designed
    pass
    
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Data and model checkpoints directoriess
    parser.add_argument('--batch-size', type=int, default=64, metavar='batch_size', help='input batch size for training (default: 64)')
    parser.add_argument('--sample-factor', type=float, default=0.1, help='sampling factor for dataset (default: 0.1)')
    parser.add_argument('--epochs', type=int, default=10, metavar='epochs', help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR', help='learning rate (default: 0.01)')
    parser.add_argument('--wd', type=float, default=0.00001, help='learning rate (default: 0.01)')
    parser.add_argument('--seed', type=int, default=1, metavar='S', help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=100, metavar='N', help='how many batches to wait before logging training status')
    parser.add_argument('--backend', type=str, default=None, help='backend for distributed training (tcp, gloo on cpu and gloo, nccl on gpu)')
    parser.add_argument('--model_path', type=str)

    # Container environment
    parser.add_argument('--hosts', type=list, default=json.loads(os.environ['SM_HOSTS']))
    parser.add_argument('--current-host', type=str, default=os.environ['SM_CURRENT_HOST'])
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--output-data-dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
    parser.add_argument('--data-dir', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    parser.add_argument('--num-gpus', type=int, default=os.environ['SM_NUM_GPUS'])

    
    bucket = 'gf-global-dev-mfg-adapt-bucket'
    prefix = 'sagemaker_autoencoder/devansh/save_model/'
    
    model = train(parser.parse_args())
    save_model(model=model, bucket=bucket, path=prefix, name='autoncoder_model_m1i1.pth')