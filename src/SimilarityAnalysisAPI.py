from subprocess import call
call("pip install scikit-image --no-cache-dir".split(" "))
call("pip install snowflake-connector-python --no-cache-dir".split(" "))
call("pip install dataframe_image --no-cache-dir".split(" "))



import pandas as pd
import numpy as np
from datetime import datetime
import glob
import boto3
import snowflake.connector
from src.train_deploy import *
from boto3 import client
import s3fs
import dataframe_image as dfi
from IPython.display import display
from urllib.parse import urlparse
import json
import os 

workers = os.cpu_count()

fs = s3fs.S3FileSystem(anon=False)


class SimilarityAnalysis:
    def __init__(self, s3path=None, savePath='s3://gf-global-dev-mfg-adapt-bucket/SimilarityAnalysis-autoaves/'):
        
        if s3path[-1] == '/':
            s3path = s3path[:-1]
        if savePath[-1] == '/':
            savePath = savePath[:-1]
            
        modelPath='s3://gf-global-dev-mfg-adapt-bucket/SimilarityAnalysis-autoaves/models/'
        
        temp = s3path[5:].split('/')
        self.s3path=s3path+'/'
        
        o = urlparse(self.s3path+'overview.json', allow_fragments=False)
        s3.download_file(o.netloc,o.path[1:],'./temp/overview.json')
        with open('temp/overview.json') as f:
            overview_file = json.load(f)
        
        self.savePath=savePath+'/'
        self.bucket = temp[0]
        self.topoFiles = []
        self.tech = overview_file['process_tag']
        self.layer = overview_file['pattern_layer_tag']
        self.maskSet = overview_file['mask_set_title']
        self.ptrf = overview_file['product_code']
        self.savePath=savePath+'/'+ self.maskSet + '/'
        self.modelName=self.tech.upper()+'_'+self.layer.upper()+'_'+'_MODEL.pt'
        
        
        try:
            self.localModelPath = 'temp/'+self.modelName
            o = urlparse(modelPath+self.modelName, allow_fragments=False)
            s3.download_file(o.netloc,o.path[1:],'./'+self.localModelPath)
        except:
            print('WARNING: Model not found in database, using default model !!')
            if len(self.layer) > 2:
                self.localModelPath = 'temp/MxIx_Model.pt'
            else:
                self.localModelPath = 'temp/Cx_Model.pt'
            
            
        for each_file in fs.glob(self.s3path+'features/toposig_prop*'):
            self.topoFiles.append('s3://'+each_file)
    
        
        # Set up logging
        self.logfile = self.maskSet+'-'+self.layer+'-'+self.tech+'-'+'{}.log'.format(datetime.datetime.now())
        logging.basicConfig(filename='./logs/'+self.logfile, level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
        logging.info('Logging started....')
        logging.info('Extracted Product name : {}'.format(self.maskSet))
        logging.info('Extracted Product code : {}'.format(self.ptrf))
        logging.info('Extracted Product tech : {}'.format(self.tech))
        logging.info('Extracted Product layer : {}'.format(self.layer))
        logging.info('Data Path : {}'.format(self.s3path))
        logging.info('Save Path : {}'.format(self.savePath))
        
        
        print('Extracted Product name : {}'.format(self.maskSet))
        print('Extracted Product code : {}'.format(self.ptrf))
        print('Extracted Product tech : {}'.format(self.tech))
        print('Extracted Product layer : {}'.format(self.layer))
        print('Data Path : {}'.format(self.s3path))
        print('Save Path : {}'.format(self.savePath))
        print('Logfile : {}'.format(self.logfile))
        print('Found {} topo_signature files:'.format(len(self.topoFiles)))
        for file in self.topoFiles:
            print(file)
        print('Initialization complete !!')
        

    def generate_encodings(self, chunkSize=1000000, seeds=None, DeltaRelaxation = 1):
        logging.info('------------------------------------------------------------------------------------------------------------------------------')
        logging.info('Started Generating encodings ...')
        
        global_start = datetime.datetime.now()
    
        filename=self.maskSet+'-'+self.layer+'-'+self.tech+'-'+'encodings.csv'
        df_enc = pd.DataFrame()
        self.k = len(seeds)

        columns = ['pattern_hash', 'topology_signature', 'x_deltas', 'y_deltas', 'pat_counts']
        filtering = False
        
        if seeds!=None:
            seed_df = pd.DataFrame()
            print('Finding seeds in database ...')
            
            chunk = pd.read_csv('s3://gf-global-dev-mfg-adapt-bucket/SimilarityAnalysis-autoaves/seeds/fab1_major_seeds.csv').drop('Unnamed: 0', axis=1)
            
            seed_df=seed_df.append(chunk[chunk['pattern_hash'].isin(seeds)])
            seed_df=seed_df.drop_duplicates(subset='pattern_hash')
            seed_df=seed_df.set_index('pattern_hash')
            if len(seed_df)==len(seeds):
                print('Found all seeds ...')
                filtering = True
                x_high = max(seed_df['x_deltas'].str.split().str.len())
                y_high = max(seed_df['y_deltas'].str.split().str.len())
                x_low = min(seed_df['x_deltas'].str.split().str.len())
                y_low = min(seed_df['y_deltas'].str.split().str.len())
            
            
            if filtering == False:
                for topo_file in self.topoFiles:
                    for chunk in pd.read_csv(topo_file, chunksize=25000000, names=columns):
                        seed_df=seed_df.append(chunk[chunk['pattern_hash'].isin(seeds)])
                        seed_df=seed_df.drop_duplicates(subset='pattern_hash')
                        if len(seed_df)==len(seeds):
                            print('Found all seeds ...')
                            filtering = True
                            x_high = max(seed_df['x_deltas'].str.split().str.len())
                            y_high = max(seed_df['y_deltas'].str.split().str.len())
                            x_low = min(seed_df['x_deltas'].str.split().str.len())
                            y_low = min(seed_df['y_deltas'].str.split().str.len())
                            break
                    if filtering == True:
                        break
        
        print('Initializing Model ...')
        encoder=Encoder(350)
        model_config = encoder.initialize_model(load_model=True, path=self.localModelPath)
        print('Model Initialization complete ...')
        logging.info('Model Initialization complete ...')
        print('Starting inference on chunks of size {}'.format(chunkSize))
        logging.info('Starting inference on chunks of size {}'.format(chunkSize))
        
        print(x_low-DeltaRelaxation, x_high+DeltaRelaxation, y_low-DeltaRelaxation, y_high+DeltaRelaxation)
                
        fileCounter = 1
        start=datetime.datetime.now()
        for topo_file in self.topoFiles:
            count = 1
            print('Processing topo_file {}/{}'.format(fileCounter, len(self.topoFiles)))
            logging.info('Processing topo_file {}/{}'.format(fileCounter, len(self.topoFiles)))
            fileCounter+=1
            for chunk in pd.read_csv(topo_file, chunksize=chunkSize, names=columns):
                chunk = chunk.set_index('pattern_hash')
                print('Processing Chunk : {}'.format(count))
                logging.info('Processing Chunk : {}'.format(count))
                count += 1
                if filtering == True:
                    chunk = ReadDF().filter_df(chunk, x_low-DeltaRelaxation, x_high+DeltaRelaxation, y_low-DeltaRelaxation, y_high+DeltaRelaxation)
                print('Data filtered, Records after filtering : {}'.format(chunk.shape[0]) )
                chunk=encoder.get_encodings(chunk, model_config=model_config, batch_size=256, num_workers=workers)
                df_enc = df_enc.append(chunk)
                #print(df_enc.shape)
                print('Chunk Processed, Time Elapsed {: .1f} min'.format((datetime.datetime.now()-start).total_seconds()/60))
                logging.info('Chunk Processed, Time Elapsed {: .1f} min'.format((datetime.datetime.now()-start).total_seconds()/60))
                    
        chunk=encoder.get_encodings(seed_df, model_config=model_config, batch_size=256, num_workers=workers)
        df_enc = df_enc.append(chunk)
        print('TOTAL TIME ELAPSED:{}'.format((datetime.datetime.now()-global_start).total_seconds()/60))
        logging.info('TOTAL TIME ELAPSED:{}'.format((datetime.datetime.now()-global_start).total_seconds()/60))
        print('Saving file: '+self.savePath+filename)
        logging.info('Saving file: '+self.savePath+filename)
        df_enc.to_csv(self.savePath+filename)
        self.enc_file = self.savePath+filename
        print('Process Completed !!')
        logging.info('Process Completed !!')

        
    def find_similar_patterns(self, seeds, k, dist_graph=False, bins=30):
        logging.info('------------------------------------------------------------------------------------------------------------------------------')
        logging.info('Started Clustering ...')
        print('Started Clustering ...')
        df_encodings = pd.read_csv(self.enc_file)
        df_encodings = df_encodings.set_index('pattern_hash')
        self.k = k
        start=datetime.datetime.now()
        encoder=Encoder(350)
        self.pathDict = {}
        for seed in seeds:
            if dist_graph == True:
                pat1=encoder.get_similar_patterns(df=df_encodings, pattern_hash=seed, k=len(df_encodings))
                dist = pat1['distance'].value_counts(bins=30).sort_index().to_frame().style.bar()
                #dfi.export(dist,self.product+'-'+self.layer+'-'+self.tech+'_'+seed+"_distance_vs_count_graph.png")
                display(dist)
                pat1 = pat1[:k]
            else:
                pat1=encoder.get_similar_patterns(df=df_encodings, pattern_hash=seed, k=k)
            #print(pat1['distance'].head(50))
            filename = self.maskSet+'-'+self.layer+'-'+self.tech+'_'+seed+'_'+'top'+str(k)+'_SimilarityAnalysis.csv'
            pat1['distance'].to_csv(self.savePath+filename)
            self.pathDict[seed] = filename
        
        print('\nSaved file S3 locations:')
        logging.info('Saved file S3 locations:')
        for key, value in self.pathDict.items():
            print(key+' : '+self.savePath+value)
            logging.info(key+' : '+self.savePath+value)
            
        print('\nProcess Completed !!')
        logging.info('Process Completed !!')
        print('Similarity Analysis complete !!')
        
    
    def getPatternLocations(self, one_location_per_seed=False):
        logging.info('------------------------------------------------------------------------------------------------------------------------------')
        pattern_list = pd.DataFrame()
        for key, value in self.pathDict.items():
            df = pd.read_csv(self.savePath+value)
            pattern_list = pd.concat([pattern_list, df['pattern_hash']])
        columns = ['pattern_hash', 'lower_x', 'lower_y', 'upper_x', 'upper_y', 'pattern_orientation']
        df_loc = pd.DataFrame()
        print('Finding pattern locations ...')
        logging.info('Finding pattern locations ...')

        try:
            temp = pd.read_csv(self.s3path+'locations/loi.csv.gz', names=columns)
            temp = temp[temp['pattern_hash'].isin(pattern_list[0])]
            df_loc = pd.concat([df_loc, temp])
        except:
            for i in range(1,1000):
                try:
                    count = str(i).zfill(5)
                    print('Searching - '+self.s3path+'locations/loi.part{}.csv.gz'.format(count))
                    logging.info('Searching - '+self.s3path+'locations/loi.part{}.csv.gz'.format(count))
                    temp = pd.read_csv(self.s3path+'locations/loi.part{}.csv.gz'.format(count), names=columns)
                    temp = temp[temp['pattern_hash'].isin(pattern_list[0])]
                    df_loc = pd.concat([df_loc, temp])
                except:
                    break
        df_loc = df_loc.assign(u_cx=lambda x: (x['upper_x']+x['lower_x'])/2)
        df_loc = df_loc.assign(u_cy=lambda x: (x['upper_y']+x['lower_y'])/2)
        if one_location_per_seed==True:
            df_loc.drop_duplicates(subset ="pattern_hash", inplace = True)
        self.locFilename = self.maskSet+'-'+self.layer+'-'+self.tech+'_'+str(self.k)+'_locations.csv'
        df_loc.to_csv(self.savePath+self.locFilename)
        print('\nSaved file to : ',self.savePath+self.locFilename)
        logging.info('Saved file to : '+self.savePath+self.locFilename)

        
    def download(self):
        logging.info('------------------------------------------------------------------------------------------------------------------------------')
        logging.info('Downloading files')
        for key, value in self.pathDict.items():
            o = urlparse(self.savePath+value, allow_fragments=False)
            s3.download_file(o.netloc,o.path[1:],'./'+value)
            print()
            logger.info('Downloaded - '+value)
        try:
            o = urlparse(self.savePath+self.locFilename, allow_fragments=False)
            s3.download_file(o.netloc,o.path[1:],'./'+self.locFilename)
            print()
            logging.info('Downloaded - '+self.locFilename)
            print('Downloaded - '+self.locFilename)
        except:
            print('No location file present, please run getPatternLocations() to fetch location of patterns')
            logging.info('No location file present, please run getPatternLocations() to fetch location of patterns')
            
        print('\nProcess Completed !!')
        logging.info('\nProcess Completed !!')
    

    def most_risky_patterns():
        print('Not yet implemented')